# -*- coding: utf-8 -*-

import _enigma_md5
supportCiphers = {'MD5' : _enigma_md5.cipher,
                  'AES-256': False}


class cipher:
    def __init__(self, cipherName):
        if not cipherName in supportCiphers:
            raise Exception('Unrecognized cipher.')
        self.worker = supportCiphers[cipherName]()
        self.keySize = self.worker.getKeySize()
        self.blockSize = self.worker.getBlockSize()

    def setKey(self,key):        
        if len(key) != self.keySize:
            raise Exception('Key size incorrect.')
        self.worker.setKey(key)

    def encrypt(self, plaintext, key=False):
        if key:
            self.setKey(key)
        if len(plaintext) != self.blockSize:
            raise Exception('Block size incorrect.')
        return self.worker.encrypt(plaintext)

    def decrypt(self, ciphertext, key=False):
        if key:
            self.setKey(key)
        if len(ciphertext) != self.blockSize:
            raise Exception('Block size incorrect.')
        return self.worker.decrypt(ciphertext)


if __name__ == '__main__':
    import time

    from modes.CBC import CBC as mode
    from modes import *
    begin = time.time()

    c = cipher('MD5')
    s = 'abcdefgh01234567' * 256 * 1024
    e = mode(c, 'a' * 16)
    ct = e.encrypt(s, getRandomString(16))
    
    mid = time.time()

    dt = e.decrypt(ct)

    end = time.time()

    print dt == s
    print 'Speed of Encryption (bytes/second): %f' % (len(s) * 1.0 / (mid - begin))
    print 'Speed of Decryption (bytes/second): %f' % (len(ct) * 1.0 / (end-mid))
