# -*- coding: utf-8 -*-

import random

def getRandomString(length):
    r = ''
    for i in xrange(length):
        r += chr(random.randint(0,255))
    return r
