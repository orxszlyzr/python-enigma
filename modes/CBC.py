# -*- coding: utf-8 -*-

class CBC:
    def __init__(self, choosenCipher, key):
        if type(choosenCipher) == str:
            self.cipher = cipher(choosenCipher)
        else:
            self.cipher = choosenCipher
        if len(key) != self.cipher.keySize:
            raise Exception('Key size must be %d bytes.' % self.cipher.keySize)
        self.cipher.setKey(key)
    
    def encrypt(self, plaintext, iv):
        if len(plaintext) % self.cipher.blockSize != 0:
            raise Exception('Plaintext length must be a multiple of %d bytes.' %
                            self.cipher.blockSize)
        if type(iv) != str or len(iv) != self.cipher.blockSize:
            raise Exception('IV must be of %d bytes in length.' %
                            self.cipher.blockSize)
        ciphertext = iv
        xorbuffer = [ ord(i) for i in iv ]
        ptbuffer = []
        loopMax = len(plaintext) / self.cipher.blockSize
        for i in xrange(loopMax):
            cutStart = i * self.cipher.blockSize
            ptbuffer = [ord(plaintext[cutStart + j]) for j in xrange(self.cipher.blockSize)]
            for j in xrange(self.cipher.blockSize):
                ptbuffer[j] ^= xorbuffer[j]
            xorbuffer = self.cipher.encrypt(ptbuffer)
            ciphertext += ''.join([chr(i) for i in xorbuffer])
        return ciphertext

    def decrypt(self, ciphertext):
        if len(ciphertext) % self.cipher.blockSize != 0:
            raise Exception('Ciphertext length must be a multiple of %d bytes.' %
                            self.cipher.blockSize)
        iv = ciphertext[0:self.cipher.blockSize]
        ciphertext = ciphertext[self.cipher.blockSize:]
        xorbuffer = [ ord(i) for i in iv ]
        ptbuffer = []
        loopMax = len(ciphertext) / self.cipher.blockSize
        plaintext = ''
        for i in xrange(loopMax):
            cutStart = i * self.cipher.blockSize
            ctbuffer = [ord(ciphertext[cutStart + j]) for j in xrange(self.cipher.blockSize)]
            ptbuffer = self.cipher.decrypt(ctbuffer)
            for j in xrange(self.cipher.blockSize):
                ptbuffer[j] ^= xorbuffer[j]
            xorbuffer = ctbuffer
            plaintext += ''.join([chr(i) for i in ptbuffer])
        return plaintext
