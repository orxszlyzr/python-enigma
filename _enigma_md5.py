# -*- coding: utf-8 -*-

# WARNING
#  DO NOT use this in combination with operation modes other than OFB and CTR!

import hashlib

class cipher:
    def __init__(self):
        pass
    def getBlockSize(self):
        return 16
    def getKeySize(self):
        return 16
    def setKey(self, key):
        chain = hashlib.md5(key).digest()
        self.key = [ord(i) for i in chain]
    def encrypt(self, pChain):
        return [ self.key[i] ^ pChain[i] for i in xrange(len(self.key)) ]
    def decrypt(self, cChain):
        return self.encrypt(cChain)
